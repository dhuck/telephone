#include "menu.h"
#include "telephone.h" // necessary for sd card pins
#include <unity.h>

Menu *empty_menu;
Menu *built_menu; // this is the menu that is built from the file, assumes same
                  // data as data folder

void setUp() {
  SPI.setMOSI(SDCARD_MOSI_PIN);
  SPI.setSCK(SDCARD_SCK_PIN);
  if (!(SD.begin(SDCARD_CS_PIN))) {
    while (true) {
      Serial.println("Unable to access the SD card");
      delay(500);
    }
  }
  empty_menu = new Menu();
  // built_menu = new Menu("test_data.tsv");
  // set stuff up here
}

void tearDown(void) {
  // clean stuff up here
}

void test_sanity(void) { TEST_ASSERT_EQUAL(1, 1); }

void test_menuRoot(void) {
  Option *root = empty_menu->returnRoot();
  TEST_ASSERT_EQUAL(0, root->id);
  TEST_ASSERT_EQUAL(0, root->pos);
  TEST_ASSERT_EQUAL(0, root->parent_id);
  TEST_ASSERT_EQUAL_STRING("root.wav", root->waveFile);
}

void test_construct_menu(void) {
  auto menu = new Menu("test_data.tsv");
  TEST_ASSERT_NOT_NULL(menu);
  TEST_ASSERT_EQUAL(6, menu->getCount());
  auto root = menu->returnRoot();
  TEST_ASSERT_NOT_NULL(root);
  TEST_ASSERT_EQUAL(0, root->id);
  auto selection = menu->makeSelection(1);
  TEST_ASSERT_NOT_NULL(selection);
  TEST_ASSERT_EQUAL(1, selection->id);
  selection = menu->makeSelection(1);
  TEST_ASSERT_NOT_NULL(selection);
  TEST_ASSERT_EQUAL(11, selection->id);
  selection = menu->makeSelection(1);
  TEST_ASSERT_NULL(selection);
}

void test_newOption(void) {
  Option *opt = newOption(1, 1, 0, "TEST.WAV");
  TEST_ASSERT_EQUAL(1, opt->id);
  TEST_ASSERT_EQUAL(1, opt->pos);
  TEST_ASSERT_EQUAL(0, opt->parent_id);
  TEST_ASSERT_EQUAL_STRING("TEST.WAV", opt->waveFile);
}

int runUnityTests(void)

{
  UNITY_BEGIN();
  RUN_TEST(test_sanity);
  RUN_TEST(test_menuRoot);
  RUN_TEST(test_newOption);
  RUN_TEST(test_construct_menu);
  return UNITY_END();
}

// int main(int argc, char **argv)
// {
//     return runUnityTests();
// }

void setup() {
  // setup here
  delay(2000); // service delay
  UNITY_BEGIN();

  runUnityTests();
}

void loop() {
  // run the tests
}