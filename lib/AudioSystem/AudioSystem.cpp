//
// Created by dhuck on 7/4/22.
//

#include "AudioSystem.h"

AudioSystem::AudioSystem(AudioPlaySdWav* sd_play,
                AudioRecordQueue* queue1)  : mode(0) {
    counter = SD.open("counter.txt");
    counter.read(&this->counter_value, 2);
    Serial.printf("AudioSystem initialized! counter_value: %d\n", counter_value);
    counter.close();
    this->queue1 = queue1;
    this->sd_play = sd_play;
};


void AudioSystem::update_counter() {
    this->counter_value += 1;
    Serial.printf("Updated counter: %d\n", counter_value);
    // if (SD.exists("counter.txt")) {
    //     SD.remove("counter.txt");
    // }
    counter = SD.open("counter.txt", FILE_WRITE_BEGIN);
    counter.write(counter_value);
    counter.flush();
    counter.close();
}

int AudioSystem::get_mode() {
    return mode;
}

void AudioSystem::continue_recording() {
    if (queue1->available() >= 2) {
        byte buffer[512];

        // Fetch 2 blocks from the audio library and copy into a 512 byte buffer.  
        // The Arduino SD library is most efficient when full 512 byte sector 
        // size writes are used.
        memcpy(buffer, queue1->readBuffer(), 256);
        queue1->freeBuffer();
        memcpy(buffer+256, queue1->readBuffer(), 256);
        queue1->freeBuffer();
        
        // write all 512 bytes to the SD card
        output_file.write(buffer, 512);
    }
}

void AudioSystem::start_recording() {
    Serial.println("startRecording");
    if (SD.exists("RECORD.RAW")) {
        SD.remove("RECORD.RAW");
    }
    output_file = SD.open("RECORD.RAW", FILE_WRITE);
    if (output_file) {
        queue1->begin();
        mode = 1;
    }
}

void AudioSystem::stop_recording() {
    Serial.println("stopRecording");
    queue1->end();
    if (mode == 1) {
        while (queue1->available() > 0) {
            output_file.write((byte *) queue1->readBuffer(), 256);
            queue1->freeBuffer();
        }
        output_file.close();
    }
    this->write_wav();
    mode = 0;
}

void AudioSystem::write_wav() {

    // make wav file name
    this->update_counter();
    String out_name = String(counter_value) + ".WAV";
    char name_buf[16];
    out_name.toCharArray(name_buf, 16);
    Serial.printf("Writing to %s\n", out_name); 

    // open files and get block size
    File input_file = SD.open("RECORD.RAW", O_RDONLY);
    File output_file = SD.open(name_buf, O_WRITE);

    uint32_t n_blocks = input_file.size() / BLOCK_SIZE;
    uint32_t remainder = input_file.size() - (n_blocks * BLOCK_SIZE);

    // write header
    wav_hdr header;

    header.ChunkSize = input_file.size() + sizeof(wav_hdr) - 8;
    header.Subchunk2Size = input_file.size() + sizeof(wav_hdr) - 44; 

    output_file.write((const char*) &header, sizeof(wav_hdr));

    // copy the chunks
    for (uint32_t i = 0; i < n_blocks; i++) {
        input_file.read(buffer, BLOCK_SIZE);
        output_file.write(buffer, BLOCK_SIZE);
    }
    if (remainder) {
        input_file.read(buffer, remainder);
        output_file.write(buffer, BLOCK_SIZE);
    }

    input_file.close();
    output_file.close();
}

bool AudioSystem::play(const char *fn) {
    if (!sd_play->isPlaying()) {
        bool result = sd_play->play(fn);
        while (!sd_play->isPlaying()) {

        }
        return result;
    }
    return false;
}

bool AudioSystem::stop() {
    if (sd_play->isPlaying()) {
        sd_play->stop();
        return true;
    }
    return false;
}

bool AudioSystem::is_playing() {
    return sd_play->isPlaying();
}

bool AudioSystem::is_recording() {
    return this->mode == 1;
}

