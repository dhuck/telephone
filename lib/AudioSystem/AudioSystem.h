//
// Created by dhuck on 7/4/22.
//

#ifndef TELEPHONE_AUDIOSYSTEM_H
#define TELEPHONE_AUDIOSYSTEM_H
#define BLOCK_SIZE 65536 // 64K;

#include <Arduino.h>
#include <Audio.h>
#include <SD.h>

#include "wav.h"

//// GUI tool: begin automatically generated code
// AudioPlaySdWav           playSdWav1;     //xy=276,380
// AudioRecordQueue         queue1;         //xy=387,578
// AudioOutputI2SQuad       i2s_s1;      //xy=666,488
////AudioConnection          patchCord1(adc1, queue1);
// AudioConnection          patchCord2(playSdWav1, 0, i2s_s1, 0);
// AudioConnection          patchCord3(playSdWav1, 1, i2s_s1, 1);
// AudioControlSGTL5000     sgtl5000_1;
//// GUI tool: end automatically generated code

class AudioSystem {
private:
  File output_file;
  File counter;
  char buffer[BLOCK_SIZE];
  uint16_t counter_value;
  int mode;

  AudioPlaySdWav *sd_play;  // xy=276,380
  AudioRecordQueue *queue1; // xy=387,578

  void update_counter();

public:
  AudioSystem(AudioPlaySdWav *sd_play, AudioRecordQueue *queue1);

  void start_recording();
  void start_recording(int FileNum);
  void continue_recording();
  void stop_recording();
  void write_wav();
  int get_mode();

  bool play(const char *fn);
  bool is_playing();
  bool is_recording();
  bool stop();
};

#endif // TELEPHONE_AUDIOSYSTEM_H
