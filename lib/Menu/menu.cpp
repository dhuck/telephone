//
// Created by dhuck on 7/17/22.
//

#include "menu.h"

Option *newOption(uint16_t _id, uint8_t _pos, uint16_t _parent_id,
                  const String &fileName) {
  auto *opt = new Option;
  opt->id = _id;
  opt->pos = _pos;
  opt->parent_id = _parent_id;
  fileName.toCharArray(opt->waveFile, MAX_FILENAME_LEN);

  for (auto &child : opt->children) {
    child = nullptr;
  }
  return opt;
}

Menu::Menu()
    : root(nullptr), currRoot(nullptr), listHead(nullptr), listTail(nullptr) {
  auto *opt = newOption(0, 0, 0, "root.wav");
  addOption(opt);
  root = currRoot = opt;
}

Menu::Menu(const char *rootFile)
    : root(nullptr), currRoot(nullptr), listHead(nullptr), listTail(nullptr) {
  parseFile(rootFile);
  // printList();
  buildMenu();
}

void Menu::parseLine(String line) {
  // get id
  auto next_tab = line.indexOf('\t');
  uint16_t id = line.substring(0, next_tab).toInt();
  line = line.substring(next_tab + 1);

  // get parent_id
  next_tab = line.indexOf('\t');
  uint16_t parent_id = line.substring(0, next_tab).toInt();
  line = line.substring(next_tab + 1);

  // get pos
  next_tab = line.indexOf('\t');
  uint8_t pos = line.substring(0, next_tab).toInt();

  // remainder is the wavefilename
  const String waveFileName = line.substring(next_tab + 1).trim();

  // build the buildQueue
  auto *opt = newOption(id, pos, parent_id, waveFileName);
  addOption(opt);
}

void Menu::parseFile(const char *rootFile) {
  File in_file = SD.open(rootFile);
  String line = in_file.readStringUntil('\n'); // ditch first line
  line = in_file.readStringUntil('\n');
  while (!line.equals("")) {
    parseLine(line);
    line = in_file.readStringUntil('\n');
  }
}

void Menu::addOption(Option *opt) {
  count++;
  if (listHead == nullptr && listTail == nullptr) {
    listHead = listTail = opt;
    return;
  }
  if (listTail != nullptr)
    listTail->next = opt;
  listTail = opt;
}

void Menu::printOption(Option *opt) {
  if (opt == nullptr)
    return;

  Serial.printf("\t%d\t%d\t%d\t", opt->id, opt->pos, opt->parent_id);
  Serial.println(opt->waveFile);
}

void Menu::printList() {
  auto curr = listHead;
  while (curr != nullptr) {
    printOption(curr);
    curr = curr->next;
  }
  Serial.println();
}

Option *Menu::getOptionById(uint16_t id) {
  auto *curr = listHead;
  while (curr != nullptr && curr->id != id) {
    curr = curr->next;
  }
  return curr;
}

void Menu::buildMenu() {
  root = currRoot = getOptionById(0);

  auto *curr = listHead;
  while (curr != nullptr) {
    if (curr->id == 0) {
      curr = curr->next;
      continue;
    }
    auto *parent = getOptionById(curr->parent_id);
    auto position = curr->pos;
    parent->children[position] = curr;
    curr = curr->next;
  }
}

Option *Menu::makeSelection(uint8_t pos) {
  Option *selection;
  if (pos == 0) {
    selection = root;
  } else {
    selection = currRoot->children[pos];
  }
  currRoot = selection;
  return selection;
}

Option *Menu::returnRoot() { return makeSelection(0); }

int Menu::getCount() { return count; }