//
// Created by dhuck on 7/16/22.
//

#ifndef TELEPHONE_SOUND_H
#define TELEPHONE_SOUND_H

#include <Arduino.h>
#include <Audio.h>

#include "menu.h"
typedef struct Sound {
  AudioPlaySdWav *sd_play;
  AudioRecordQueue *queue;
  File output_file;
  uint8_t mode;
} Sound;

void startRecording(Sound *as);
void continueRecording(Sound *as);
void stopRecording(Sound *as);
uint8_t getMode(Sound *as);

bool play(Sound *as, Option *opt);
bool isPlaying(Sound *as);
bool stop(Sound *as);

#endif  // TELEPHONE_SOUND_H