//
// Created by dhuck on 7/4/22.
//

#ifndef TELEPHONE_MENU_H
#define TELEPHONE_MENU_H

#include <Arduino.h>
#include <SD.h>

constexpr uint8_t NUM_CHILDREN = 10;
constexpr uint8_t MAX_FILENAME_LEN = 16;

typedef struct Option {
  Option *children[NUM_CHILDREN]{};
  char waveFile[MAX_FILENAME_LEN]{};
  Option *next = nullptr;
  uint16_t id{};
  uint16_t parent_id{};
  uint8_t pos{};
} Option;

Option *newOption(uint16_t _id, uint8_t _pos, uint16_t _parent_id,
                  const String &fileName);

class Menu {
private:
  Option *root;
  Option *currRoot;
  Option *listHead;
  Option *listTail;
  int count = 0;

  void parseCell(String &line, uint8_t &next_tab, String &cell);
  void parseFile(const char *rootFile);
  void parseLine(String line);
  void addOption(Option *opt);
  Option *getOptionById(uint16_t id);

  void printOption(Option *opt);
  void printList();
  void buildMenu();

public:
  explicit Menu();
  explicit Menu(const char *rootFile);
  Option *makeSelection(uint8_t pos);
  Option *returnRoot();
  int getCount();
};

#endif // TELEPHONE_MENU_H
