//
// Created by dhuck on 7/4/22.
//

#ifndef TELEPHONE_TELEPHONE_H
#define TELEPHONE_TELEPHONE_H

#include <Arduino.h>
#include <SD.h>

#include "AudioSystem.h"
#include "menu.h"

#define HOOK_SWITCH 0
#define ROTARY_ENABLE 4
#define ROTARY_COUNTER 2

// Use these with the Teensy Audio Shield
#ifdef TEENSY40
#define SDCARD_CS_PIN 10
#define SDCARD_MOSI_PIN 7
#define SDCARD_SCK_PIN 14
#else
// use for Teensy 4.1/3.6
#define SDCARD_CS_PIN BUILTIN_SDCARD
#define SDCARD_MOSI_PIN 11 // not actually used
#define SDCARD_SCK_PIN 13  // not actually used
#endif

#define ROTARY_DEBOUNCE_TIME 60
#define ENABLE_DEBOUNCE_TIME 60

constexpr uint8_t NUM_OPTIONS = 10;
constexpr uint16_t DELAY_TIME = 1200;
constexpr uint16_t MAX_SILENCE = 5000;

class Telephone {
private:
  char empty_string[MAX_FILENAME_LEN];
  AudioSystem *as;
  Menu *menu;
  volatile uint8_t enabled;
  volatile uint8_t last_dialed;
  volatile uint8_t report;
  volatile uint8_t count;
  volatile uint32_t silence;
  volatile uint8_t last_rotary_disabled;
  volatile bool greeting;
  volatile bool record;
  volatile bool restart;
  volatile uint8_t rotary;

public:
  explicit Telephone(AudioSystem *);
  void enable_phone();
  void enable_rotary();
  void disable_rotary();
  void disable_phone();
  bool is_enabled();
  void wedding_loop();
  void menu_loop();

  void print_directory(File dir, uint8_t num_tabs, bool hidden);
  void play_greeting();
  void record_msg();
  void print_report(bool force);
  void phone_number_count();
  void phone_number();
  bool makeSelection(uint8_t num);
  void monitor();
};

#endif // TELEPHONE_TELEPHONE_H
