//
// Created by dhuck on 7/3/22.
//
#include "telephone.h"

Telephone::Telephone(AudioSystem *as)
    : enabled(0), last_dialed(11), report(0), count(0), silence(0),
      last_rotary_disabled(0), greeting(false) {
  enabled = digitalRead(HOOK_SWITCH) ? 0 : 1;
  restart = true;
  for (char &i : empty_string) {
    i = '\0';
  }
  this->as = as;
  this->menu = new Menu(
      "test_data.tsv"); // TODO: you best make this a variable or sum shit
  this->disable_phone();
}

void Telephone::print_report(bool force) {
  if ((report == 1) || force) {
    Serial.print("Hook is currently: ");
    if (enabled) {
      Serial.print("ENABLED! >>>> ");
    } else {
      Serial.print("DISABLED! >>>> ");
    }
    if (last_dialed > 10) {
      Serial.print("No numbers dialed yet.");
    } else {
      Serial.print("Last number dialed: ");
      Serial.print(last_dialed % 10);
    }
    Serial.println();
    report = 0;
  }
}

void Telephone::print_directory(File dir, uint8_t num_tabs, bool hidden) {
  File entry = dir.openNextFile();
  while (entry) {
    for (uint8_t i = 0; i < num_tabs; i++) {
      Serial.print('\t');
    }
    if (!hidden && entry.name()[0] == '.') {
      entry = dir.openNextFile();
      continue;
    }
    Serial.print(entry.name());
    if (entry.isDirectory()) {
      Serial.println("/");
      print_directory(entry, num_tabs + 1, hidden);
    } else {
      Serial.print("\t\t");
      Serial.println(entry.size(), DEC);
    }

    entry.close();
    entry = dir.openNextFile();
  }
  dir.close();
}

void Telephone::phone_number_count() {
  noInterrupts();
  this->count++;
  interrupts();
}

void Telephone::enable_rotary() {
  noInterrupts();
  count = 0;
  interrupts();
}

void Telephone::disable_rotary() {
  last_dialed = count;
  makeSelection(last_dialed);
}

void Telephone::play_greeting() {
  if (as->is_playing())
    return;

  delay(750);
  greeting = true;
  Serial.println("playing greeting message");
  as->play("GREETING.WAV");
}

void Telephone::record_msg() {
  if (as->is_playing())
    return;

  if (as->is_recording()) {
    as->continue_recording();
    return;
  }
  as->start_recording();
}

void Telephone::phone_number() {
  if (!digitalRead(ROTARY_ENABLE)) {
    uint32_t curr_disable = millis();
    uint32_t diff = curr_disable - last_rotary_disabled;
    if (diff > ENABLE_DEBOUNCE_TIME && count > 0) {
      last_dialed = count;
      makeSelection(last_dialed);
      report = count;
      count = 0;
    }
  } else {
    last_rotary_disabled = millis();
  }
}

void Telephone::enable_phone() {
  this->enabled = true;
  delay(250);
  auto *selection = menu->returnRoot();
  as->play(selection->waveFile);
}

void Telephone::disable_phone() {
  if (as->is_playing())
    as->stop();
  if (as->is_recording())
    as->stop_recording();
  this->greeting = false;
  this->enabled = false;
  this->record = false;
}

bool Telephone::is_enabled() { return enabled == 1; }

bool Telephone::makeSelection(uint8_t num) {
  Serial.printf("makeSelection %d\n", num);
  if (!enabled || num >= NUM_OPTIONS)
    return false;
  auto *selection = menu->makeSelection(num);
  if (selection == nullptr) { // end of the run, time for goodbye
    Serial.println("selection is null");
    return true;
  } else {
    silence = 0;
    if (as->is_playing())
      as->stop();

    as->play(selection->waveFile);
    if (selection->children[1] == nullptr) {
      return true;
    }
  }
  return false;
}

// void Telephone::monitor() {
//   enabled = this->is_enabled();
//   if (!enabled && as->is_playing()) {
//     as->stop();
//   }
//   if (!enabled && as->is_recording()) {
//     as->stop_recording();
//     Serial.println("stopRecording");
//   }
//   if (as->is_playing() || !enabled) {
//     return;
//   }
//   if (restart) {
//     menu->makeSelection(0);
//     restart = false;
//   }
//   if (count > 0) {
//     Serial.println(count);
//     phone_number();
//   } else if (as->is_recording()) {
//     record_msg();
//   } else if (greeting) {
//     delay(DELAY_TIME);
//     as->play("GOODBYE.WAV");
//     enabled = 0;
//   } else if (!as->is_playing() && silence == 0) {
//     silence = millis();
//   } else if (!as->is_playing() && (millis() - silence) >= MAX_SILENCE) {
//     silence = 0;
//     makeSelection(0);
//   }
// }

// void Telephone::menu_loop() {
//   if (!enabled)
//     return;
// }

void Telephone::wedding_loop() {
  if (!enabled)
    return;

  if (!greeting)
    play_greeting();
  record_msg();
}
