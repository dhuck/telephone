#include <Arduino.h>
#include <Audio.h>
#include <Bounce2.h>
#include <SD.h>
#include <SPI.h>
#include <SerialFlash.h>
#include <Wire.h>
#include <avr/interrupt.h>
#include <avr/io.h>

#include "telephone.h"

// GUItool: begin automatically generated code
// TODO: Figure out why this is not working in this configuration
//  whenever I switch over to using AudioInputI2S, the output no longer
//  plays. Whenever it is using the AnalogInput, things work fine...

// GUItool: begin automatically generated code
AudioInputI2S i2s2;       // xy=105,63
AudioRecordQueue queue1;  // xy=281,63
AudioPlaySdWav playSdWav; // xy=302,157
AudioOutputI2S i2s1;      // xy=470,120
AudioMixer4 mixer;
AudioConnection patchCord1(playSdWav, 0, mixer, 0);
AudioConnection patchCord2(playSdWav, 1, mixer, 1);
AudioConnection patchCord5(mixer, 0, i2s1, 0);
AudioConnection patchCord6(mixer, 0, i2s1, 1);
AudioConnection patchCord4(i2s2, 0, queue1, 0);
AudioControlSGTL5000 sgtl5000_1; // xy=265,212
// GUItool: end automatically generated code

File root;
elapsedMillis msecs;
AudioSystem *as;
Telephone *t;

Bounce HOOK = Bounce();
Bounce COUNTER = Bounce();
Bounce COUNTER_ENABLE = Bounce();

void phone_number_count() {
  static unsigned long last_interrupt = 0;
  unsigned long curr_interrupt = millis();
  if (t->is_enabled() &&
      curr_interrupt - last_interrupt >= ROTARY_DEBOUNCE_TIME) {
    t->phone_number_count();
  }
  last_interrupt = curr_interrupt;
}

void check_hook() {
  HOOK.update();
  if (HOOK.fallingEdge()) {
    Serial.println("Off the hook!");
    t->enable_phone();
  } else if (HOOK.risingEdge()) {
    Serial.println("Turning phone off!");
    t->disable_phone();
  }
}

void check_rotary() {
  if (!t->is_enabled()) {
    return;
  }
  COUNTER_ENABLE.update();
  if (COUNTER_ENABLE.risingEdge()) {
    t->enable_rotary();
  } else if (COUNTER_ENABLE.fallingEdge()) {
    t->disable_rotary();
  }
}

void setup() {
  AudioMemory(8);
  pinMode(ROTARY_COUNTER, INPUT_PULLDOWN);

  COUNTER_ENABLE.attach(ROTARY_ENABLE, INPUT_PULLDOWN);
  COUNTER_ENABLE.interval(ENABLE_DEBOUNCE_TIME);
  HOOK.attach(HOOK_SWITCH, INPUT_PULLDOWN);
  HOOK.interval(15);

  // mount the sd card. If it fails, print message repeatedly
  SPI.setMOSI(SDCARD_MOSI_PIN);
  SPI.setSCK(SDCARD_SCK_PIN);

  if (!(SD.begin(SDCARD_CS_PIN))) {
    while (true) {
      Serial.println("Unable to access the SD card");
      delay(500);
    }
  }

  // set up the rotary
  attachInterrupt(digitalPinToInterrupt(ROTARY_COUNTER), phone_number_count,
                  FALLING);

  Serial.begin(9600);

  as = new AudioSystem(&playSdWav, &queue1);
  t = new Telephone(as);

  mixer.gain(0, 0.757);
  mixer.gain(1, 0.757);
  mixer.gain(2, 0.757);

  sgtl5000_1.enable();
  sgtl5000_1.volume(0.4);
  sgtl5000_1.inputSelect(AUDIO_INPUT_MIC);
  sgtl5000_1.micGain(18);

  // start when monitor opens
  delay(1000);
  Serial.println("Hello, and Welcome to Telephone!");
  Serial.println("Set up complete, starting.");
}

void sanity() {
  static unsigned long last = 0;
  static const uint16_t SANITY = 30000; // ten seconds
  unsigned long curr = millis();
  if (curr - last > SANITY) {
    Serial.println("Awake!");
    last = curr;
  }
}

void loop() {
  check_hook();
  check_rotary();
}
